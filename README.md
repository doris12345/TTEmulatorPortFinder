# TTEmulatorPortFinder
Used to find the android emulator adb port and automatically connect, such as Tencent mobile assistant, Netease MuMu、BlueStacksNox、App Player and so on.

用于找到当前各类Android/安卓模拟器的adb端口并自动连接，支持模拟器多开时端口查找 ，理论上支持所有模拟器端口搜寻，诸如腾讯手游助手、网易MuMu、BlueStacks、海马、夜神、天天、Genymotion等。

# 使用方法
首次使用先安装依赖：
```
pip install -r requirements.txt
```

使用Python3.6及其以上版本运行。
```
python emulator_port.py
```
运行完成后会自动打印出可连接端口，如果只有一个模拟器可连接会自动使用adb连接上模拟器。